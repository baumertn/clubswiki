package converter

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"path"
	"sort"
	"strconv"

	_ "github.com/mattn/go-sqlite3" // Using sqlite with default database/sql
)

type datapoint struct {
	URL       string
	Occurance int
	Score     float32
}

func (data datapoint) ToString() string {
	return data.URL + ";" + strconv.Itoa(data.Occurance) + ";" + strconv.FormatFloat(float64(data.Score), 'g', 10, 32) + ";"
}

// Start runs the converter. This will take the temporary database, build the final data and write it to a TSV file.
func Start(dir string) {
	totalOccurances := make(map[string]int)
	redirects := make(map[string]string)
	loadRedirects(redirects, path.Join(dir, "clubswiki-redirects.csv"))
	tmpDB, err := sql.Open("sqlite3", path.Join(dir, "clubswiki.tmp.db"))
	if err != nil {
		fmt.Println(err)
	}

	var str string
	var lastStr string
	var url string
	urls := make(map[string]int)

	outFile, err := os.Create(path.Join(dir, "Clubswiki.db"))
	if err != nil {
		fmt.Println(err)
	}
	outCSV := csv.NewWriter(outFile)
	outCSV.Comma = '\t'
	rows, err := tmpDB.Query("SELECT `lnrm`, `wikilink` FROM `link_data` ORDER BY `lnrm` ASC;")
	if err != nil {
		fmt.Println(err)
	}
	defer rows.Close()
	fmt.Println("Convert data...")
	rows.Next()
	err = rows.Scan(&str, &url)
	if err != nil {
		fmt.Println(err)
	}
	lastStr = str
	urls[url]++
	for rows.Next() {
		err = rows.Scan(&str, &url)
		if err != nil {
			fmt.Println(err)
		}
		// Handling redirects.
		if redirect, ok := redirects[url]; ok {
			if redirect == "__DISAMBIGUATION__" || redirect == "__NAME_DISAMBIGUATION__" {
				url = ""
			} else {
				url = redirect
			}
		}
		if str == lastStr {
			if url != "" {
				urls[url]++
				totalOccurances[str]++
			}
		} else {
			var data []datapoint
			for u, occurances := range urls {
				data = append(data, datapoint{u, occurances, float32(occurances) / float32(totalOccurances[lastStr])})
			}
			// Sort by score, highest to lowest.
			sort.Slice(data, func(i, j int) bool {
				return data[i].Score > data[j].Score
			})
			for _, d := range data {
				outCSV.Write([]string{lastStr, d.URL, strconv.Itoa(d.Occurance), strconv.FormatFloat(float64(d.Score), 'g', 10, 32)})
			}
			urls = make(map[string]int)
			if url != "" {
				urls[url]++
				totalOccurances[str]++
			}
		}
		lastStr = str
		outCSV.Flush()
	}
	outCSV.Flush()
	fmt.Println("... done.")
}

func loadRedirects(redirects map[string]string, file string) {
	// redirects := make(map[string]string)
	redirectsFile, err := os.Open(file)
	if err != nil {
		fmt.Println(err)
	}
	defer redirectsFile.Close()
	redirectReader := csv.NewReader(redirectsFile)
	redirectReader.Comma = '\t'
	for {
		line, err := redirectReader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Println(err)
		}
		url := line[0]
		target := line[1]
		redirects[url] = target
	}
}
