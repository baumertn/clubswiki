# Clubswiki
A reimplementation of [Crosswikis](https://nlp.stanford.edu/pubs/crosswikis.pdf).
The name comes from playing cards as Clubs = Kreuz in German and Kreuz more literally translates to cross.

The system was first created in Python3 as part of my Bachelor Project. This reimplementation represents my first steps into Go, so it's a learning experience for me. Don't expect decent Go code.

## Usage
To use Clubswiki you will need the Wikipedia dataset. You can get the [torrent](https://meta.wikimedia.org/wiki/Data_dump_torrents#English_Wikipedia) and more [information](https://en.wikipedia.org/wiki/Wikipedia:Database_download#English-language_Wikipedia). Clubswiki uses the compressed bz2-file, let's call it `wikipedia.xml.bz2`.
```
go get gitlab.com/baumertn/clubswiki
clubswiki wikipedia.xml.bz2 output/ [-keep] [-pagelimit n]
```
- `output/` is just a directory where all the temporary and final data will be stored.
- The `-keep`-flag will keep the temporary SQLite database around. This can be useful for debugging, or if you want to run your own converter.
- The `-pagelimit`-Flag is a debug flag, if it is provided with a number `n` the parser will parse `n` pages from the Wikipedia-XML and then stop.

## Customizing Clubswiki
The Clubswiki parser allows for some customization. You can change way how strings and urls are normalized and formatted. For that you simply overwrite the public functions `StrNormalize` and `URLNormalize`, which both have the signature `func (s string) string`.

You can also add to the list of attributes that are extracted from infoboxes by pushing to the `InfoboxParameters` string slice. This method can lead to problems if different infoboxes have the same attribute but you want to handle it differently.