package main

import (
	"fmt"
	"os"
	"path"

	flags "github.com/jessevdk/go-flags"
	"gitlab.com/baumertn/clubswiki/converter"
	"gitlab.com/baumertn/clubswiki/parser"
)

var opts struct {
	Arguments struct {
		Wikipedia string `positional-arg-name:"wikipedia" description:"the Wikipedia XML .bz2"`
		Output    string `positional-arg-name:"output" description:"output directory"`
	} `required:"true" positional-args:"true"`
	Keep      bool `short:"k" long:"keep" description:"Do not delete the temporary database."`
	PageLimit int  `short:"l" long:"limit" default:"-1" description:"Debug: Only handle the given number of pages and stop."`
}

func main() {
	_, err := flags.Parse(&opts)
	if err != nil {
		panic(err)
	}
	parser.PageLimit = opts.PageLimit
	parser.Start(opts.Arguments.Wikipedia, opts.Arguments.Output)
	converter.Start(opts.Arguments.Output)
	if !opts.Keep {
		fmt.Println("Deleting the temporary database...")
		os.Remove(path.Join(opts.Arguments.Output, "clubswiki.tmp.db"))
		fmt.Println("... done.")
	}
}
