package parser

import (
	"encoding/csv"
	"os"
	"testing"

	"github.com/m-m-f/gowiki"
)

func TestStrNormalization(t *testing.T) {
	table := []struct {
		str    string
		result string
	}{
		{"Hello, World!", "helloworld"},
		{"! ? : ; . , - _  \" ' \\ ( ) { } [ ] # \t \n", ""},
		{"", ""},
	}
	for _, data := range table {
		expected := data.result
		got := normalize(data.str)
		if got != expected {
			t.Errorf("Expected %s, got: %s", expected, got)
		}
	}
}

func TestUrlNormalization(t *testing.T) {
	table := []struct {
		str    string
		result string
	}{
		{"Hello, World!", "Hello,_World!"},
		{"Barack Obama", "Barack_Obama"},
		{"", ""},
		{" ", "_"},
	}
	for _, data := range table {
		expected := data.result
		got := wikinize(data.str)
		if got != expected {
			t.Errorf("Expected %s, got: %s", expected, got)
		}
	}
}

func TestLinkParsing(t *testing.T) {
	db := make(map[string]string)
	insertKeyValue := func(key, value string) error {
		db[key] = value
		return nil
	}

	links := []gowiki.WikiLink{
		gowiki.WikiLink{"", "", ""},
		gowiki.WikiLink{"", "Empty Anchor Test", ""},
		gowiki.WikiLink{"", "Anchor Test", "Anchor"},
		gowiki.WikiLink{"", "", "Empty Page"}, // This can not happen, if gowiki does it's job. But an empty string can be the result of normalization.
		// TODO: More tests.
	}

	_, err := parseLinks(insertKeyValue, links)
	if err != nil {
		t.Errorf("Parsing the links failed with error %s", err)
	}
	for _, link := range links {
		if link.HasAnchor() {
			if _, ok := db[normalize(link.Anchor)]; !ok {
				t.Errorf("Key %s not in database", normalize(link.Anchor))
			}
			got := db[normalize(link.Anchor)]
			expected := wikinize(link.PageName)
			if got != expected {
				t.Errorf("Expected %s, got: %s", expected, got)
			}
		} else {
			if _, ok := db[normalize(link.PageName)]; !ok {
				t.Errorf("Key %s not in database", normalize(link.PageName))
			}
			got := db[normalize(link.PageName)]
			expected := wikinize(link.PageName)
			if got != expected {
				t.Errorf("Expected %s, got: %s", expected, got)
			}
		}
	}
}

func TestTemplateParsing(t *testing.T) {
	db := make(map[string]string)
	csvWriter := csv.NewWriter(os.Stdout)
	insertKeyValue := func(key, value string) error {
		db[key] = value
		return nil
	}

	templates := []*gowiki.Template{
		&gowiki.Template{"normal", "Disambiguation", "", nil},
		&gowiki.Template{"normal", "disambiguation", "", nil},
		&gowiki.Template{"normal", "disambiiguation", "", nil},
		&gowiki.Template{"normal", "Infobox test", "", map[string]string{"name": "Test.Name"}},
		&gowiki.Template{"normal", "Infobox test", "", map[string]string{"name": "This Great Book", "title_orig": "Das unfassbare Buch", "working_title": "Das gute Buch", "isbn": "978-3-86680-192-9"}},
	}

	for _, template := range templates {
		temps := []*gowiki.Template{template}
		_, err := parseTemplates(insertKeyValue, csvWriter, temps, "Test")
		if err != nil {
			t.Errorf("Parsing the template failed with error %s", err)
		}
	}
	expected := "Test"
	got := db[normalize("Test.Name")]
	if got != expected {
		t.Errorf("Expected %s, got: %s", expected, got)
	}
	got = db[normalize("This Great Book")]
	if got != expected {
		t.Errorf("Expected %s, got: %s", expected, got)
	}
	got = db[normalize("Das unfassbare Buch")]
	if got != expected {
		t.Errorf("Expected %s, got: %s", expected, got)
	}
	got = db[normalize("Das gute Buch")]
	if got != expected {
		t.Errorf("Expected %s, got: %s", expected, got)
	}
	got = db[normalize("978-3-86680-192-9")]
	if got != expected {
		t.Errorf("Expected %s, got: %s", expected, got)
	}

}

// typ: "normal"
// "Infobox test"
// "name": ""
// "title_orig", "working_title", "isbn",
// 	// Infobox film
// 	"film_name",
// 	// Infobox musical artist
// 	"alias",
// 	// Infobox song
// 	"English_title",
// 	// Infobox television
// 	"show_name", "show_name_2", "native_name",
// 	// Infobox person
// 	"native_name", "birth_name", "birthname", "full_name", "fullname",
// 	// Infobox royalty
// 	"title",
// 	// Infobox company
// 	"trading_name", "romanized_name", "ISIN",
