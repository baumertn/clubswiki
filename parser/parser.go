package parser

import (
	"compress/bzip2"
	"database/sql"
	"encoding/csv"
	"encoding/xml"
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/m-m-f/gowiki"
	_ "github.com/mattn/go-sqlite3" // Using sqlite with default database/sql bindings.
)

// StrNormalize defines the way the strings are normalized.
var StrNormalize = normalize

// URLNormalize defines the way the Wikipedia URL parts are formated.
var URLNormalize = wikinize

// PageLimit is a debug value that breaks the Wikipedia parsing after PageLimit pages have been parsed. -1 parses all pages.
var PageLimit = -1

// InfoboxParameters is a string slice containing all parameters the relate to the current page.
var InfoboxParameters = []string{
	// General parameter
	"name",
	// Infobox book
	"title_orig", "working_title", "isbn",
	// Infobox film
	"film_name",
	// Infobox musical artist
	"alias",
	// Infobox song
	"English_title",
	// Infobox television
	"show_name", "show_name_2", "native_name",
	// Infobox person
	"native_name", "birth_name", "birthname", "full_name", "fullname",
	// Infobox royalty
	"title",
	// Infobox company
	"trading_name", "romanized_name", "ISIN",
}

type redirect struct {
	Target string `xml:"title,attr"`
}

type page struct {
	Title string   `xml:"title"`
	Redir redirect `xml:"redirect"`
	Text  string   `xml:"revision>text"`
	NS    int      `xml:"ns"`
}

func normalize(str string) string {
	norm := strings.ToLower(str)
	for _, c := range [...]string{"!", "?", "\"", "'", ":", ";", ".", ",", "-", "_", " ", "\\", "(", ")", "{", "}", "[", "]", "#", "\t", "\n"} {
		norm = strings.Replace(norm, c, "", -1)
	}
	return norm
}

func wikinize(str string) string {
	wiki := strings.Replace(str, " ", "_", -1)
	return wiki
}

// Start runs the parser on "in" and puts all resulting files into "out".
func Start(in, out string) {
	// Open the xml file.
	bzFile, err := os.Open(in)
	var pageCounter int
	if err != nil {
		fmt.Println(err)
	}
	defer bzFile.Close()
	tmpDB, err := sql.Open("sqlite3", path.Join(out, "clubswiki.tmp.db"))
	if err != nil {
		fmt.Println(err)
	}
	defer tmpDB.Close()
	// Create table
	create, err := tmpDB.Prepare("CREATE TABLE IF NOT EXISTS `link_data` (`lnrm`	TEXT NOT NULL, `wikilink` TEXT NOT NULL);")
	if err != nil {
		fmt.Println(err)
	}
	_, err = create.Exec()
	if err != nil {
		fmt.Println(err)
	}
	insert, err := tmpDB.Prepare("INSERT INTO `link_data` (lnrm, wikilink) VALUES (?, ?);")
	if err != nil {
		fmt.Println(err)
	}

	redirectFile, err := os.Create(path.Join(out, "clubswiki-redirects.csv"))
	if err != nil {
		fmt.Println(err)
	}
	defer redirectFile.Close()
	reader := bzip2.NewReader(bzFile)
	redirectWriter := csv.NewWriter(redirectFile)
	redirectWriter.Comma = '\t'
	decoder := xml.NewDecoder(reader)
	var counter int
	tx, err := tmpDB.Begin()
	if err != nil {
		fmt.Println(err)
	}
	for {
		// Read tokens from XML in a stream
		t, _ := decoder.Token()
		if t == nil {
			break
		}
		// Check the token
		switch el := t.(type) {
		case xml.StartElement:
			if el.Name.Local == "page" {
				pageCounter++
				var p page
				decoder.DecodeElement(&p, &el)
				// Filter namespaces
				if p.NS != 0 { // 0 = Main/Article, see: https://en.wikipedia.org/wiki/Wikipedia:Namespace
					continue
				}
				// Store Title -> Page relations
				_, err = tx.Stmt(insert).Exec(StrNormalize(p.Title), URLNormalize(p.Title))
				if err != nil {
					fmt.Println(err)
				}
				counter++
				// Store redirects
				if p.Redir.Target != "" {
					err := redirectWriter.Write([]string{URLNormalize(p.Title), URLNormalize(p.Redir.Target)})
					if err != nil {
						fmt.Println(err)
					}
					redirectWriter.Flush()
				}
				parsed, err := gowiki.ParseArticle(p.Title, p.Text, &gowiki.DummyPageGetter{})
				if err != nil {
					fmt.Println(err)
				}
				count, err := parseLinks(insertTransaction(tx, insert), parsed.Links)
				if err != nil {
					fmt.Println(err)
				}
				counter += count
				count, err = parseTemplates(insertTransaction(tx, insert), redirectWriter, parsed.Templates, p.Title)
				if err != nil {
					fmt.Println(err)
				}
				counter += count
				// parsed.GenText() // Makes parsed.Text available.
				// Batching
				if counter%50000 == 0 {
					tx.Commit()
					tx, err = tmpDB.Begin()
					if err != nil {
						fmt.Println(err)
					}
				}
			}
		}
		if PageLimit > -1 && pageCounter >= PageLimit {
			break
		}
	}
	tx.Commit()
	redirectWriter.Flush()
	fmt.Println("Finished", pageCounter, "Pages.")
	fmt.Println("Stored", counter, "relations.")
	fmt.Println("Generate index...")
	// Generate Index
	index, err := tmpDB.Prepare("CREATE INDEX IF NOT EXISTS `link_data_lnrm_index` ON `link_data` (`lnrm`	ASC, `wikilink`);")
	if err != nil {
		fmt.Println(err)
	}
	_, err = index.Exec()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("... done.")
}

func insertTransaction(tx *sql.Tx, statement *sql.Stmt) func(string, string) error {
	return func(key string, value string) error {
		_, err := tx.Stmt(statement).Exec(key, value)
		if err != nil {
			return err
		}
		return nil
	}
}

func parseLinks(insert func(string, string) error, links []gowiki.WikiLink) (int, error) {
	var counter int
	for _, link := range links {
		if checkIllegalLinkPrefix(link.PageName) {
			continue
		}
		if link.HasAnchor() {
			err := insert(StrNormalize(link.Anchor), URLNormalize(link.PageName))
			if err != nil {
				return counter, err
			}
		} else {
			err := insert(StrNormalize(link.PageName), URLNormalize(link.PageName))
			if err != nil {
				return counter, err
			}
		}
		counter++
	}
	return counter, nil
}

func parseTemplates(insert func(string, string) error, redirectWriter *csv.Writer, templates []*gowiki.Template, title string) (int, error) {
	var counter int
	for _, template := range templates {
		if strings.Contains(template.Name, "Infobox") || strings.Contains(template.Name, "infobox") {
			// TODO: Infobox categories could be extracted here.
			for _, parameter := range InfoboxParameters {
				if result, ok := template.Parameters[parameter]; ok {
					err := insert(StrNormalize(result), URLNormalize(title))
					if err != nil {
						return counter, err
					}
					counter++
				}

			}
		} else if strings.Contains(template.Name, "Disambiguation") || strings.Contains(template.Name, "disambiguation") {
			err := redirectWriter.Write([]string{URLNormalize(title), "__DISAMBIGUATION__"})
			if err != nil {
				return counter, err
			}
			redirectWriter.Flush()
		} else if strings.Contains(template.Name, "given name") || strings.Contains(template.Name, "surname") {
			err := redirectWriter.Write([]string{URLNormalize(title), "__NAME_DISAMBIGUATION__"})
			if err != nil {
				return counter, err
			}
			redirectWriter.Flush()
		}
	}
	return counter, nil
}

// Filters internal wikilinks. Returns true if a illegal wikilink prefix has been found. See: https://www.mediawiki.org/wiki/Help:Links#Internal_links
func checkIllegalLinkPrefix(link string) bool {
	return (strings.HasPrefix(link, ":Category:") ||
		strings.HasPrefix(link, "Category:") ||
		strings.HasPrefix(link, ":File:") ||
		strings.HasPrefix(link, ":Media:") ||
		strings.HasPrefix(link, "File:") ||
		strings.HasPrefix(link, "Special:"))
}
